from django import views
from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index),
    path('articles/2003/', views.special_case_2003),
    re_path('articles/(?P<year>[0-9]{4})/$', views.year_archive),
    path('articles/<int:year>/<int:month>/', views.month_archive),
    path('articles/<int:year>/<int:month>/<slug:slug>/', views.article_detail),
]




